<?php

namespace Elly;

/* PSR-0 Autoloader implementation
 * @package Elly
 */
class ClassAutoloader
{

    /**
    * Installs this class loader on the SPL autoload stack.
    */
    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }
     
    /**
    * Uninstalls this class loader from the SPL autoloader stack.
    */
    public function unregister()
    {
        spl_autoload_unregister(array($this, 'loadClass'));
    }
     
    /**
    * Loads the given class or interface.
    *
    * @param string $className The name of the class to load.
    * @return void
    */
    public function loadClass($className)
    {
        $className = ltrim($className, '\\');
        $fileName  = '';
        $namespace = '';
        if ($lastNsPos = strrpos($className, '\\')) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }
        $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

        require_once $fileName;
    }
}