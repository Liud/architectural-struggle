<?php

namespace Elly\Foundation;
use Elly\Foundation\Routing\Interfaces\RouterInterface;

/**
 * Application starter class
 * To avoid "tight coupling" and bring to user an ability 
 * to easy change the parts of application we using dependency injection.
 * 
 * @package Elly\Foundation
 */
class Application
{
    /**
     * Router instance
     * @var Object Router
     */
    protected $router;

    /**
     * Dispatcher instanse
     * @var Object Dispatcher
     */
    protected $dispatcher;

    public function __construct(RouterInterface $router, $dispatcher)
    {
        $this->router = $router;
        $this->dispatcher = $dispatcher;
    }

    /**
     * We starting our application by running dispatcher
     * I belive in future it can do a much more than only dispatch routes :) 
     */
    public function run($request, $response)
    {
        $this->dispatcher->dispatch($this->router, $request, $response);
    }
}