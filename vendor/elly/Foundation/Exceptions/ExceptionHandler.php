<?php

namespace Elly\Foundation\Exceptions;

use Elly\Foundation\Exceptions\Interfaces\ExceptionInterface;

class ExceptionHandler
{
	public function __construct()
	{
		set_exception_handler(array($this, 'exception'));
	}

    public function exception($exception){
        echo $exception->getMessage();
    }
}