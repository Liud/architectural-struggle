<?php

namespace Elly\Foundation\Http;
use Elly\Foundation\Http\Interfaces\RequestInterface;
/**
 * Small request handler
 * @package Elly
 */
class Request implements RequestInterface
{

    /**
     * @var
     */
    protected $uri;
    /**
     * @var array
     */
    protected $params = array();
    /**
     * @var
     */
    protected $method;
    /**
     * @var
     */
    protected $server;
    /**
     * @var
     */
    protected static $content;

    public function __construct()
    {
        $this->server = $_SERVER;
        $this->parseRequest();
        $this->parseContent();
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function setParam($key, $value)
    {
        $this->params[$key] = $value;
        return $this;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getParam($key)
    {
        if (!isset($this->params[$key])) {
            throw new \InvalidArgumentException("The request parameter with key '$key' is invalid.");
        }
        return $this->params[$key];
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return self::$content;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     *
     */
    protected function parseRequest()
    {
        $this->uri = trim(parse_url($this->server["REQUEST_URI"], PHP_URL_PATH), "/");
        foreach ($_REQUEST as $name => $value) {
            $this->setParam($name, $value);
        }
        $this->method = $this->server['REQUEST_METHOD'];
    }

    /**
     * @return string
     */
    protected function parseContent()
    {

        if (null === self::$content) {
            self::$content = file_get_contents('php://input');
        }
        return self::$content;
    }
}