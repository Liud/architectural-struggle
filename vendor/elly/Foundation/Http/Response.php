<?php

namespace Elly\Foundation\Http;


/**
 * Small renspose handler
 * http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
 * @package Elly\Foundation\Http
 */
/**
 * Class Response
 * @package Elly\Foundation\Http
 */
class Response
{

    /**
     *
     */
    const HTTP_OK = 200;
    /**
     *
     */
    const HTTP_BAD_REQUEST = 400;
    /**
     *
     */
    const HTTP_FORBIDDEN = 403;
    /**
     *
     */
    const HTTP_NOT_FOUND = 404;
    /**
     *
     */
    const HTTP_INTERNAL_SERVER_ERROR = 500;
    /**
     *
     */
    const HTTP_NOT_IMPLEMENTED = 501;


    /**
     * @var array
     */
    protected $status_texts = array(
        200 => 'OK',
        400 => 'Bad Request',
        403 => 'Forbidden',
        404 => 'Not Found',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
    );

    /**
     * @var string
     */
    protected $content;

    /**
     * @var integer
     */
    protected $status_code;

    /**
     * @var array
     */
    protected $headers;

    /**
     * @var integer
     */
    protected $protocol_version = '1.0';

    public function __construct($content = '', $status_code = 200, $headers = array())
    {
        $this->content = $content;
        $this->status_code = $status_code;
        $this->headers = $headers;
    }


    /**
     * @param $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status_code = $status;
        return $this;
    }

    /**
     * @param $headers
     * @return $this
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @param $version
     * @return $this
     */
    public function setVersion($version)
    {
        $this->protocol_version = $version;
        return $this;
    }


    /**
     *
     */
    public function send()
    {
        $this->sendHeaders();
        $this->sendContent();
    }

    /**
     *
     */
    protected function sendHeaders()
    {
        if (!headers_sent()) {
            header(
                sprintf(
                    'HTTP/%s %s %s',
                    $this->protocol_version,
                    $this->status_code,
                    $this->status_texts[$this->status_code]
                ),
                true,
                $this->status_code
            );
            foreach ($this->headers as $name => $value) {
                header($name . ': ' . $value, false, $this->status_code);
            }
        }
    }

    /**
     *
     */
    protected function sendContent()
    {
        echo $this->content;
    }
}