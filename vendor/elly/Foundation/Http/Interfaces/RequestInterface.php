<?php

namespace Elly\Foundation\Http\Interfaces;

/**
 * Interface RequestInterface
 * @package Elly\Foundation\Http\Interfaces
 */
interface RequestInterface
{
    /**
     * Get uri
     * @return mixed
     */
    public function getUri();

    /**
     * Set parameters
     * @param $key
     * @param $value
     * @return mixed
     */
    public function setParam($key, $value);

    /**
     * Get parameter
     * @param $key
     * @return mixed
     */
    public function getParam($key);

    /**
     * Get parameters
     * @return mixed
     */
    public function getParams();

    /**
     * Get content
     * @return mixed
     */
    public function getContent();

    /**
     * Get method POST, GET..
     * @return mixed
     */
    public function getMethod();
}