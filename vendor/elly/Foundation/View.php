<?php

namespace Elly\Foundation;

/**
 * Class View
 * @package Elly\Foundation
 */
class View
{
    /**
     * @var string
     */
    protected $views_bast_path = 'App\Views';

    /**
     *
     */
    public function __construct()
    {

    }

    /**
     * @param $views_bast_path
     */
    public function setViewsPath($views_bast_path)
    {
        $this->views_bast_path = $views_bast_path;
    }

    /**
     * @param $filename
     * @param array $data
     * @return string
     */
    public function renderPartrial($filename, $data = array())
    {
        extract($data);
        ob_start();
        $this->requireView($filename);
        $content = ob_get_contents();
        ob_clean();
        return $content;
    }

    /**
     * @param $filename
     * @param array $data
     */
    public function render($filename, $data = array())
    {
        $this->requireView($filename);
    }

    /**
     * @param $filename
     */
    protected function requireView($filename)
    {
        require $this->views_bast_path . DIRECTORY_SEPARATOR . $filename . '.php';
    }
}