<?php

namespace Elly\Foundation\Routing;

use Elly\Foundation\Routing\Interfaces\RouteInterface;

/**
 * Simple router
 * @package Elly\Foundation\Routing
 */
class Route implements RouteInterface
{

    /**
     * Expected method
     * POST, GET, PUT...
     * @var string
     */
    protected $method;

    /**
     * Controller action
     * @var string
     */
    protected $action;

    /**
     * Expected uri
     * @var string
     */
    protected $url_path;

    /**
     * Controller that will be called
     * @var string
     */
    protected $controller;

    /**
     * Validation rules for params
     * example: :id => [0-9]
     * @var array
     */
    protected $params_patterns = array();

    /**
     * Parsed params
     * @var array
     */
    protected $params = array();

    public function __construct($method, $url_path, $controller_path)
    {
        $this->method = $method;
        $this->url_path = $url_path;
        $this->parseControllerPath($controller_path);
    }

    /**
     * Return controller name
     * @return string
     */
    public function getClass()
    {
        return $this->controller;
    }

    /**
     * Returns method name
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Returns uri
     * @return mixed
     */
    public function getPath()
    {
        return $this->url_path;
    }

    /**
     * Returns controller action
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Sets validation rules for params
     * @param $param_patterns
     */
    public function where($param_patterns)
    {
        $this->params_matterns = $param_patterns;
    }

    /**
     * Returns params
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Parse path to controller and action
     * @param $controller_path
     * @throws \Exception
     */
    protected function parseControllerPath($controller_path)
    {
        $path = explode('::', $controller_path);

        if (!empty($path[0])) {
            $this->controller = $path[0];
        } else {
            throw new \Exception('Class not specified');
        }

        if (!empty($path[1])) {
            $this->action = $path[1];
        } else {
            throw new \Exception('Method not specified');
        }
    }

    /**
     * Dirty hack to check if route match the request
     * @param Request
     * @param Response
     * @return boolean
     */
    public function match($request, $response)
    {

        $uri = explode('/', $request->getUri());
        $route_uri = explode('/', $this->url_path);
        $url = array_merge($uri, $route_uri);
        foreach ($route_uri as $index => $value) {
            if ($request->getMethod() !== $this->method or !isset($uri[$index])) {
                return false;
            } else {
                if (preg_match('/^:/i', $value)) {
                    if (preg_match('/' . $this->params_matterns[$value] . '/', $uri[$index])) {
                        $this->params[trim($value, ':')] = $uri[$index];
                        unset($uri[$index]);
                    } else {
                        return false;
                    }
                } else {
                    unset($uri[$index]);
                }

            }
        }
        if (!empty($uri)) {
            return false;
        }
        return true;
    }
}