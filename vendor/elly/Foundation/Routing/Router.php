<?php
namespace Elly\Foundation\Routing;

use Elly\Foundation\Routing\Interfaces\RouterInterface;
use Elly\Foundation\Routing\Interfaces\RouteInterface;
use Elly\Foundation\Http\Interfaces\RequestInterface;
/**
 * Simple class that manages routes
 * Implements "FluentInterface" pattern
 * Example usage: Router->AddRoutes()->AddRoute()
 * @package Elly\Foundation\Routing 
 */
class Router implements RouterInterface
{
    /**
     * Array of routes
     * @var array Routes
     */
    protected $routes = array();

    public function __construct($routes)
    {
        $this->addRoutes($routes);
    }

    /**
     * Add the route to router
     * @param Route Instance of Route object
     * @return Router 
     */
    public function addRoute(RouteInterface $route)
    {
        $this->routes[] = $route;
        return $this;
    }

    /**
     * Add the list of routes to router
     * @param Route Instance of Route object
     * @return Router 
     */    
    public function addRoutes(array $routes)
    {
        foreach ($routes as $route) {
            $this->addRoute($route);
        }
        return $this;
    }
 
    /**
     * Get a list of routes
     * @return Router
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Checking if route uri match request uri
     * @return Route Instance of Route class
     */
    public function match(RequestInterface $request, $response)
    {
        //$method = $request->getMethod();
        //$uri = $request->getUri();
        foreach ($this->getRoutes() as $route) {
            if ($route->match($request, $response)) {
                return $route;
            }
        }

        return false;
    }
}