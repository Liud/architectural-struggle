<?php

namespace Elly\Foundation\Routing;

/**
 * It's very simple Dispather. Of course in future in can be extended
 * do more than just dispatch routes
 *
 * Class Dispatcher
 * @package Elly\Foundation\Routing
 */
class Dispatcher
{

    /**
     *
     * @param $router
     * @param $request
     * @param $response
     * @return mixed
     * @throws \Exception
     */
    public function dispatch($router, $request, $response)
    {

        if ($route = $router->match($request, $response)) {

            $class = $route->getClass();
            $action = $route->getAction();

            $ref = new \ReflectionClass($class);
            if($ref->hasMethod($action)){
                return $ref->getMethod($action)->invokeArgs(new $class, array($route->getParams()));
            } else {
                throw new \Exception('Action '.$action.' not found in '.$class);
            }
            

        }
        return $response->setContent('Page not found')->setStatus(404)->send();
        
    }
}