<?php

namespace Elly\Foundation\Routing\Interfaces;


/**
 * Route interface
 * @package Elly
 */
/**
 * Interface RouteInterface
 * @package Elly\Foundation\Routing\Interfaces
 */
interface RouteInterface
{
    /**
     * @return mixed
     */
    public function getClass();

    /**
     * @return mixed
     */
    public function getMethod();

    /**
     * @return mixed
     */
    public function getPath();

    /**
     * @return mixed
     */
    public function getAction();

    /**
     * @return mixed
     */
    public function match($request, $response);
}