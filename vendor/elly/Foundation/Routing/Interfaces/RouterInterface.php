<?php

namespace Elly\Foundation\Routing\Interfaces;

use Elly\Foundation\Http\Interfaces\RequestInterface;

/**
 * Router interface
 * @package Elly
 */
interface RouterInterface
{
    /**
     * @param RouteInterface $route
     * @return mixed
     */
    public function addRoute(RouteInterface $route);

    /**
     * @param array $routes
     * @return mixed
     */
    public function addRoutes(array $routes);

    /**
     * @return mixed
     */
    public function getRoutes();

    /**
     * @param RequestInterface $request
     * @param $response
     * @return mixed
     */
    public function match(RequestInterface $request, $response);
}