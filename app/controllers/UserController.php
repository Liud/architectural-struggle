<?php

namespace App\Controllers;

use App\Models\User;
use App\Repositories\CsvUserRepository;
use Elly\Foundation\Http\Response;
use Elly\Foundation\Http\Request;

/**
 * Class UserController
 * @package App\Controllers
 */
class UserController
{

    /**
     * RepositoryPattern was implemented becouse there is
     * can be a case developer will want to change storage engine
     *
     * UserRepository 
     * @var UserRepository Instance of UserRepository
     */
    protected $user_repo;

    /**
     * Response
     * @var Responce Instance of Response
     */
    protected $response;

    public function __construct()
    {
        $this->user_repo = new CsvUserRepository();
        $this->response  = new Response();
        $this->request   = new Request();
    }

    /**
     *  Retrieve all users in repo
     *  @return Response
     */
    public function index()
    {
        if ($users = $this->user_repo->getAll()) {
            $this->response->setContent(json_encode($users));
        } else {
            $this->response->setContent('Store with users are empty');
        }
        $this->response->send();
    }

    /**
     * Retrieve user by id
     * @param $params Array params from uri
     */
    public function show($params)
    {
        if ($user = $this->user_repo->findById($params['id'])) {
            $this->response->setContent(json_encode($user));
        } else {
            $this->response->setStatus(404)
                           ->setContent('User not found in storage');
        }
        $this->response->send();
    }

    /**
     * Store user in repository
     * @return Response
     */
    public function store()
    {
        if($content = json_decode($this->request->getContent(), true)){
            $user = new User();
            $user->setAttributes($content);
            
            $result = $this->user_repo->store($user);
            print_r($result); die();
        }else{
            $this->response->setStatus(400)
                           ->setContent('Bad request');
        }
        $this->response->send();
    }

    /**
     * Update user in repo by id
     * @param $params Array Array of uri params 
     * @throws \Exception
     * @return Response
     */
    public function update($params)
    {
        if($content = json_decode($this->request->getContent(), true)){
            $user = new User();
            $user->setAttributes($content);
            if(!$user->validate()){
                 $this->response->setContent(json_encode($user->getErrors()))
                                ->setStatus(400);

            } else{
              if($this->user_repo->update($user, $params['id'])){
                    $this->response->setContent(json_encode($user->getAttributes()));
                } 
            }
        }else{
            $this->response->setStatus(400)
                           ->setContent('Bad request');
        }
        $this->response->send();
    }

    /**
     * Destroy user by id
     * @param $params Array of uri params 
     * @return Response
     */
    public function destroy($params)
    {
        if($this->user_repo->destroy($params['id'])){
            $this->response->setContent('User with id '.$params['id'].' was successfully destroyed');
        }
        $this->response->send();
    }

}