<?php

namespace App\Repositories;

use App\Repositories\UserRepositoryInterface;

class CsvUserRepository implements UserRepositoryInterface
{
    /**
     * Users
     * @var array
     */
    protected $users;

    /**
     * Path to storage
     * @var string
     */
    protected $storage_path = 'App\\Storages\\Csv\\example.csv';

	public function __construct()
	{
		$this->users = $this->parseCSV();
	}

    /**
     * Find user by id
     * @param $id
     * @return bool
     */
    public function findById($id)
	{
		if(isset($this->users[$id])){
			return $this->users[$id];
		}
		return false;
	}

    /**
     * Get all users
     * @return array|bool
     */
    public function getAll()
	{
		if(!empty($this->users)){
			return $this->users;
		}

		return false;
	}

    /**
     * Store user
     * @param $user
     * @return bool
     */
    public function store($user)
	{
		if($user){
			if(!empty($this->users)){
				$this->users[] = $user->getAttributes();
			} else {
				$this->users[1] = $user->getAttributes();

			}
			if ($this->storeInCSV($this->users)){
	            $user->id = max(array_keys($this->users)); //Trick to get last inserted id
	            return $user;
			}
		}
		return false;
	}

    /**
     * Update user
     * @param $user
     * @param $id
     * @return bool
     */
    public function update($user, $id)
	{
		if($user){
			if(isset($this->users[$id])){
				$this->users[$id] = $user->getAttributes();
				if($this->storeInCSV($this->users)){
					return true;
				}
			}			
		}

		return false;
	}

    /**
     * Destroy user
     * @param $id
     * @return bool
     */
    public function destroy($id)
	{
		if(isset($this->users[$id])){
			unset($this->users[$id]);
			if($this->storeInCSV($this->users)){
				return $id;
			}
		} 
		return false;
	}

    /**
     * Get normalized path to storage
     * @return mixed
     */
    protected function getNormalizedPath()
	{
		$path = str_replace('\\', DIRECTORY_SEPARATOR, $this->storage_path);
		return $path;
	}

    /**
     * Retrieving data from csv file
     * @return array
     */
    protected function parseCSV()
	{
		$users = array();
	 	$file = fopen($this->getNormalizedPath(), 'r');
	 	$i = 1;
        while (($line = fgetcsv($file)) !== false) {
            $users[$i] = array(
                'name' => $line[0],
                'phone' => $line[1],
                'street' => $line[2]
            );
            $i++;
        }
        fclose($file);
        return $users;
	}

    /**
     * Store data in csv file
     * @param $data
     * @return bool
     */
    protected function storeInCSV($data)
	{
		if($csv = fopen($this->getNormalizedPath(), "w")){

			foreach($data as $user){
				fputcsv($csv, $user);
			}
			
			fclose($csv);
            return true;
		} 
		return false;
	}
}