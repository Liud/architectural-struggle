<?php

namespace App\Repositories;

/**
 * Interface UserRepositoryInterface
 * @package App\Repositories
 */
interface UserRepositoryInterface
{
    /**
     * Id of entity
     * @param $id
     * @return mixed
     */
    public function findById($id);

    /**
     * Get all entities
     * @return mixed
     */
    public function getAll();

    /**
     * Store entity
     * @param $user
     * @return mixed
     */
    public function store($user);

    /**
     * Update entity
     * @param $user
     * @param $id
     * @return mixed
     */
    public function update($user, $id);

    /**
     * Destroy entity
     * @param $id
     * @return mixed
     */
    public function destroy($id);
}