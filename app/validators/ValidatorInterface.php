<?php

namespace App\Validators;

/**
 * Validator interface
 * @package App\Validators
 */
interface ValidatorInterface{

	/**
	 * Runs validation logic
	 * @return boolean
	 */
	public function validate($value, $name);

	/**
	 * Runs validation logic 
	 * @return string Error message string
	 */
	public function getErrorMessage();
}