<?php

namespace App\Validators;

/**
 * Class Integer
 * @package App\Validators
 */
class Integer implements ValidatorInterface
{
    /**
     * Name of attribute
     * @var String
     */
    protected $attribute_name;

    /**
     * @param $value
     * @param $name
     * @return bool
     */
    public function validate($value, $name)
	{
		if(preg_match('/[0-9]/i', $value)){
			return true;
		}else{
			$this->error_message = 'Attribute '.$name.' is not valid';
			return false;
		}
	}

    /**
     * Get error message
     * @return mixed
     */
    public function getErrorMessage()
	{
		return $this->error_message;
	}
}