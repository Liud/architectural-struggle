<?php

namespace App\Validators;

/**
 * Class String
 * @package App\Validators
 */
class String
{
    /**
     * Name of attribute
     * @var String
     */
    protected $attribute_name;

    /**
     * Validate attrubute
     * @param $value
     * @param $name
     * @return bool
     */
    public function validate($value, $name)
	{
		if(preg_match('/[a-zA-Z]/i', $value)){
			return true;
		}else{
			$this->error_message = 'Attribute '.$name.' is not valid';
			return false;
		}
	}

    /**
     * Get error message
     * @return mixed
     */
    public function getErrorMessage()
	{
		return $this->error_message;
	}
}