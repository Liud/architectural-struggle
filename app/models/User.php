<?php

namespace App\Models;

use App\Models\Model;

/**
 * Class Model
 * @package App\Models
 */
class User extends Model
{
    protected $validators_folder = 'App\Validators';

    /**
     * Validation rules
     */
    protected $validation_rules = array(
        'name' => 'string',
        'phone' => 'integer',
        'adress' => 'string'
    );
}