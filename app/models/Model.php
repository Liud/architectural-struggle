<?php 

namespace App\Models;

/**
 * Abstrat model class 
 * @package App\Models
 */
abstract class Model
{
    /**
     * Path to validators folder
     * @var string
     */
    protected $validators_folder = 'App\Validators';

    /**
     * @var array
     */
    protected $attributes = array();

    /**
     * Validation rules
     * @var array
     */
    protected $validation_rules = array();

    /**
     * Validation errors
     * @var array
     */
    protected $validation_errors = array();


	public function __set($name, $value){
		$this->attributes[$name] = $value;
	}

	public function __get ($key) {
        return $this->attributes[$key];
    }

    /**
     * Set attributes
     * @param array Array of attributes
     */
    public function setAttributes($data)
    {
    	$this->attributes = $data;
    }
    
   /**
    * Get attributes
    * @return array attributes
    */
    public function getAttributes()
    {
    	return $this->attributes;
    }

   /**
    * Get validation errors
    * @return array Validation errors
    */
    public function getErrors()
    {
    	return $this->validation_errors;
    }

    /**
     * Attributes validation
     * Just a very basic implementation
     * In ideal way we should implement "Chain of responsibility" pattern
     * @return boolean
     */
    public function validate()
    {

    	foreach ($this->attributes as $name => $value) {
    		if(isset($this->validation_rules[$name])){
    			$rule = $this->validation_rules[$name];
    			$validator_name = '\\'.$this->validators_folder.'\\'.ucfirst($this->validation_rules[$name]);
    			if($validator = new $validator_name()){
    				if(!$validator->validate($value, $name)){
    					$this->validation_errors[] = $validator->getErrorMessage();
    				}
    			}else{
    				throw new \Exception ('Validator do not exist!');
    			}

    		}
    	}

    	if(empty($this->validation_errors)){
    		return true;
    	}
    	return false;
    }
}