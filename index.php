<?php
/*
$var = 'asdads';
if(preg_match( '/^:/i', $var )){
	echo 'yes';
}else{
	echo 2;
}

die();*/
use Elly\Foundation\Application;
use Elly\Foundation\Http\Request;
use Elly\Foundation\Http\Response;
use Elly\Foundation\Routing\Route;
use Elly\Foundation\Routing\Router;
use Elly\Foundation\Routing\Dispatcher;
use Elly\Foundation\Exceptions\ExceptionHandler;
//Include autoloader, can be changed to composer autoload
if (file_exists('vendor/autoload.php')) {
    require 'vendor/autoload.php';
}


/**
 * Register autoloader
 */
$autoloader = new Elly\ClassAutoloader();
$autoloader->register();

/**
 * Creating Response and Request Instances
 */
$request = new Request();
$response = new Response();
  
/**
 * Setting up rest routes for user
 * Router('Method', 'Uri', 'Controller namespace::Action');
 * Methods: POST, GET, DELETE, PUT..etc.
 */ 

$index = new Route("GET", "users", "App\\Controllers\\UserController::index");

$show = new Route("GET", "users/:id", "App\\Controllers\\UserController::show");
$show->where(array(':id' => '[0-9]'));

$store = new Route("POST", "users", "App\\Controllers\\UserController::store");

$update = new Route("PUT", "users/:id", "App\\Controllers\\UserController::update");
$update->where(array(':id' => '[0-9]'));

$destroy = new Route("DELETE", "users/:id", "App\\Controllers\\UserController::destroy");
$destroy->where(array(':id' => '[0-9]'));



/**
 * Pass routes to router
 */  
$router = new Router(array($index, $show, $store, $update, $destroy));

/**
 * Creating instance if  Dispatcher
 */
$dispatcher = new Dispatcher();
 
/**
 * Creating instance of error handler
 */
$handler = new ExceptionHandler();

/**
 * Creating Application
 */
$app = new Application($router, $dispatcher, $handler);
$app->run($request, $response); //Run Elly, Run